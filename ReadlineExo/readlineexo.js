
const readline = require('readline');

const rl = readline.createInterface({input: process.stdin,
                                    output :process.stdout});
const tab =[];

rl.question('Combien de nombre paire doivent être lus?', (reponse) =>{
    const stop = reponse;
        rl.setPrompt(`Entrez un nombre\n`);
        rl.prompt();
        rl.on('line', (nombre)=>{
            if(nombre.trim()%2!==0){
                rl.setPrompt('Entez un nombre paire\n');
                rl.prompt();
            }
            else {
                    tab.push(nombre);
                    if(tab.length==stop){
                        rl.close();
                    }                  
                    rl.setPrompt('Entrez un nombre \n');
                    rl.prompt();                
                }   
         });   
 });

rl.on('close',()=>{
    const tab2 = tab.filter((e)=>e%3===0);
    console.log(`Merci d'avoir joué!`);
    console.log(`Voici les nombres paires divisibles par 3 : ${tab2}`);
    console.log(`il contient ${tab2.length} éléments `);
    process.exit();
});



